install:
	pip install git+ssh://git@bitbucket.org/cartwear/cartwear.git
	pip install -r requirements.txt
	python setup.py develop

upgrade:
	pip install -U django-cartwear
	pip install -U -r requirements.txt --use-mirrors
	python setup.py develop --upgrade

sandbox:
	rm -f sandbox/db.sqlite
	sandbox/manage.py syncdb --noinput
	sandbox/manage.py makemigrations --noinput
	sandbox/manage.py migrate --noinput
	sandbox/manage.py loaddata sandbox/fixtures/auth.json countries.json
	sandbox/manage.py cartwear_import_catalogue sandbox/fixtures/catalogue.csv

.PHONY: all install upgrade sandbox
