.. cartwear-payuin documentation master file, created by
   sphinx-quickstart on Wed Aug 22 20:12:56 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cartwear-payuin's documentation!
===============================================

This package provides integration between django-cartwear and PayU India payment:

* *payuin Express* - Involves redirecting customer's over to payuin's site where
  they can choose shipping options and confirm payment using their payuin
  account.  The customer is then redirected back to the merchant site where they
  confirm the order.

Further, it's possible to use this option in Django without Cartwear.

Installation
------------

Whichever payment option you wish to use, the package installation instructions
are the same.

Install::

    pip install cartwear-payuin

By default, this won't install Cartwear as well. To install Cartwear, run::

    pip install "cartwear-payuin[cartwear]"

Finally, add ``payuin`` to your ``INSTALLED_APPS``, and run::

    python manage.py syncdb

Table of contents
-----------------

.. toctree::
    :maxdepth: 2

    express
    payflow
    contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
