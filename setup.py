#!/usr/bin/env python
from setuptools import setup, find_packages

from payuin import VERSION


setup(
    name='cartwear-payuin',
    version=VERSION,
    url='https://github.com/cartwear/cartwear-payuin',
    author="Ray Ch",
    author_email="ray@ninjaas.com",
    description=(
        "Integration with PayU India"
        "Payments for django-cartwear"),
    long_description=open('README.rst').read(),
    keywords="Payment, payuin, Cartwear",
    license=open('LICENSE').read(),
    platforms=['linux'],
    packages=find_packages(exclude=['sandbox*', 'tests*']),
    include_package_data=True,
    install_requires=[
        'requests>=1.0',
        'django-localflavor'],
    extras_require={
        'cartwear': ["django-cartwear>=0.6"]
    },
    # See http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Other/Nonlisted Topic'],
)
