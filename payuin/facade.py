"""
Responsible for briding between Cartwear and the PayUIn gateway
"""
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

from payuin.models import PayUInTransaction
from payuin.gateway import (set_txn, get_txn, do_txn)


def get_payuin_url(basket, shipping_methods, user=None, shipping_address=None,
                   shipping_method=None, host=None, scheme=None,
                   payuin_params=None):
    """
    Return the URL for a PayUIn Express transaction.

    This involves registering the txn with PayUIn to get a one-time
    URL.  If a shipping method and shipping address are passed, then these are
    given to PayUIn directly - this is used within when using PayUIn as a
    payment method.
    """
    if basket.currency:
        currency = basket.currency
    else:
        currency = getattr(settings, 'PAYUIN_CURRENCY', 'GBP')
    if host is None:
        host = Site.objects.get_current().domain
    if scheme is None:
        use_https = getattr(settings, 'PAYUIN_CALLBACK_HTTPS', True)
        scheme = 'https' if use_https else 'http'
    return_url = '%s://%s%s' % (
        scheme, host, reverse('payuin-success-response', kwargs={
            'basket_id': basket.id}))
    failure_url = '%s://%s%s' % (
        scheme, host, reverse('payuin-failure-response', kwargs={
            'basket_id': basket.id}))
    cancel_url = '%s://%s%s' % (
        scheme, host, reverse('payuin-cancel-response', kwargs={
            'basket_id': basket.id}))

    # URL for updating shipping methods - we only use this if we have a set of
    # shipping methods to choose between.
    update_url = None
    if shipping_methods:
        update_url = '%s://%s%s' % (
            scheme, host,
            reverse('payuin-shipping-options',
                    kwargs={'basket_id': basket.id}))

    # Determine whether a shipping address is required
    no_shipping = False
    if not basket.is_shipping_required():
        no_shipping = True

    # Pass a default billing address is there is one.  This means PayUIn can
    # pre-fill the registration form.
    address = None
    if user:
        addresses = user.addresses.all().order_by('-is_default_for_billing')
        if len(addresses):
            address = addresses[0]

    return set_txn(basket=basket,
                   shipping_methods=shipping_methods,
                   currency=currency,
                   return_url=return_url,
                   failure_url=failure_url,
                   cancel_url=cancel_url,
                   update_url=update_url,
                   shipping_method=shipping_method,
                   shipping_address=shipping_address,
                   user=user,
                   user_address=address,
                   no_shipping=no_shipping,
                   payuin_params=payuin_params)


def fetch_transaction_details(token):
    """
    Fetch the completed details about the PayUIn transaction.
    """
    return get_txn(token)


def confirm_transaction(token, amount):
    """
    Confirm the payment action.
    """
    return do_txn(token, amount)
