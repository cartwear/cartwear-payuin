from django.conf.urls import *
from django.views.decorators.csrf import csrf_exempt

from payuin import views


urlpatterns = patterns('',
    url(r'^preview/(?P<basket_id>\d+)/$',
        views.SuccessResponseView.as_view(preview=True),
        name='payuin-success-response'),
    url(r'^cancel/(?P<basket_id>\d+)/$', views.CancelResponseView.as_view(),
        name='payuin-cancel-response'),
    url(r'^failure/(?P<basket_id>\d+)/$', views.FailureResponseView.as_view(),
        name='payuin-failure-response'),
    url(r'^place-order/(?P<basket_id>\d+)/$', views.PlaceOrderResponseView.as_view(),
        name='payuin-place-order'),
    # View for using PayUIn as a payment method
    url(r'^payment/', views.RedirectView.as_view(),name='payuin-direct-payment'),
    url(r'^redirect-payment/', views.redirectView,name='payuin-redirect-payment'),
)
