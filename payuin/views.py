from __future__ import unicode_literals
from decimal import Decimal as D
from hashlib import sha512
import logging
import json

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView, View
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.utils.http import urlencode
from django.utils import six
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist

import cartwear
from cartwear.apps.payment.exceptions import UnableToTakePayment
from cartwear.core.loading import get_class, get_model
from cartwear.apps.shipping.methods import FixedPrice, NoShippingRequired

from payuin.facade import (
    get_payuin_url, fetch_transaction_details, confirm_transaction)
from payuin.exceptions import (
    EmptyBasketException, MissingShippingAddressException,
    MissingShippingMethodException, InvalidBasket,PayUInError)

from payuin.models import PayUInTransaction

# Load views dynamically
PaymentDetailsView = get_class('checkout.views', 'PaymentDetailsView')
CheckoutSessionMixin = get_class('checkout.session', 'CheckoutSessionMixin')

ShippingAddress = get_model('order', 'ShippingAddress')
Country = get_model('address', 'Country')
Basket = get_model('basket', 'Basket')
Repository = get_class('shipping.repository', 'Repository')
Applicator = get_class('offer.utils', 'Applicator')
Selector = get_class('partner.strategy', 'Selector')
Source = get_model('payment', 'Source')
SourceType = get_model('payment', 'SourceType')

logger = logging.getLogger('payuin.express')

from django.shortcuts import render_to_response


KEYS = ('key', 'txnid', 'amount', 'productinfo', 'firstname', 'email',
        'udf1', 'udf2', 'udf3', 'udf4', 'udf5',  'udf6',  'udf7', 'udf8',
        'udf9',  'udf10')

def generate_hash(data):
    hash = sha512('')
    for key in KEYS:
        hash.update("%s%s" % (str(data.get(key, '')), '|'))
    hash.update(getattr(settings, 'PAYUIN_API_SALT', 'C0Ds8q'))
    return hash.hexdigest().lower()

def redirectView(request):
    try:
        transaction = PayUInTransaction.objects.get(txnid=request.GET['txnid'])
    except ObjectDoesNotExist:
        messages.error(request, _("Transaction ID doesn't exist"))
        url = reverse('basket:summary')
        return HttpResponseRedirect(url)
    else:
        data = transaction.request()
        data['txnid'] = transaction.txnid
        data['hash'] = generate_hash(data)
        # Construct return URL
        if getattr(settings, 'PAYUIN_SANDBOX_MODE', True):
            url = 'https://test.payu.in/_payment'
        else:
            url = 'https://secure.payu.in/_payment'
        data['url'] = url
        return render_to_response('payuin/redirect.html',data)


class RedirectView(CheckoutSessionMixin, RedirectView):
    """
    Initiate the transaction with Paypal and redirect the user
    to PayUIn's Express Checkout to perform the transaction.
    """
    permanent = False

    def get_redirect_url(self, **kwargs):
        try:
            basket = self.request.basket
            url = self._get_redirect_url(basket, **kwargs)
        except PayUInError:
            messages.error(
                self.request, _("An error occurred communicating with PayUIn"))
            url = reverse('checkout:payment-details')
            return url
        except InvalidBasket as e:
            messages.warning(self.request, six.text_type(e))
            return reverse('basket:summary')
        except EmptyBasketException:
            messages.error(self.request, _("Your basket is empty"))
            return reverse('basket:summary')
        except MissingShippingAddressException:
            messages.error(
                self.request, _("A shipping address must be specified"))
            return reverse('checkout:shipping-address')
        except MissingShippingMethodException:
            messages.error(
                self.request, _("A shipping method must be specified"))
            return reverse('checkout:shipping-method')
        else:
            # Transaction successfully registered with PayUIn.  Now freeze the
            # basket so it can't be edited while the customer is on the PayUIn
            # site.
            basket.freeze()

            logger.info("Basket #%s - redirecting to %s", basket.id, url)

            return url

    def _get_redirect_url(self, basket, **kwargs):
        if basket.is_empty:
            raise EmptyBasketException()

        params = {
            'basket': basket,
            'shipping_methods': []          # setup a default empty list
        }                                   # to support no_shipping

        user = self.request.user
        if basket.is_shipping_required():
            # Only check for shipping details if required.
            print "Shipping Required"
            shipping_addr = self.get_shipping_address(basket)
            if not shipping_addr:
                raise MissingShippingAddressException()

            shipping_method = self.get_shipping_method(
                basket, shipping_addr)
            if not shipping_method:
                raise MissingShippingMethodException()
            print  shipping_addr
            params['shipping_address'] = shipping_addr
            params['shipping_method'] = shipping_method
            params['shipping_methods'] = []


        if settings.DEBUG:
            # Determine the localserver's hostname to use when
            # in testing mode
            params['host'] = self.request.META['HTTP_HOST']

        if user.is_authenticated():
            params['user'] = user

        params['payuin_params'] = self._get_payuin_params()

        return get_payuin_url(**params)

    def _get_payuin_params(self):
        """
        Return any additional PayUIn parameters
        """
        return {}


class CancelResponseView(RedirectView):
    permanent = False

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CancelResponseView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        error_msg = _(
            "A problem occurred communicating with PayU India "
            "- please try again later"
        )

        try:
            self.token = request.POST['txnid']
        except KeyError:
            # Probably suspicious manipulation if we get here
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))

        try:
            self.txn = fetch_transaction_details(self.token)
        except PayUInError:
            # Unable to fetch txn details from PayU - we have to bail out
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))
        return self.get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        basket = get_object_or_404(Basket, id=kwargs['basket_id'],
                                   status=Basket.FROZEN)
        basket.thaw()
        logger.info("Payment cancelled (token %s) - basket #%s thawed",
                    request.GET.get('token', '<no token>'), basket.id)
        return super(CancelResponseView, self).get(request, *args, **kwargs)

    def get_redirect_url(self, **kwargs):
        messages.error(self.request, _("PayU Transaction Cancelled"))
        return reverse('basket:summary')


class FailureResponseView(RedirectView):
    permanent = False

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(FailureResponseView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        error_msg = _(
            "A problem occurred communicating with PayU India "
            "- please try again later"
        )

        try:
            self.token = request.POST['txnid']
        except KeyError:
            # Probably suspicious manipulation if we get here
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))

        try:
            self.txn = fetch_transaction_details(self.token)
        except PayUInError:
            # Unable to fetch txn details from PayU - we have to bail out
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))
        return self.get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        basket = get_object_or_404(Basket, id=kwargs['basket_id'],
                                   status=Basket.FROZEN)
        basket.thaw()
        logger.info("Payment failure (token %s) - basket #%s thawed",
                    request.GET.get('token', '<no token>'), basket.id)
        return super(FailureResponseView, self).get(request, *args, **kwargs)

    def get_redirect_url(self, **kwargs):
        messages.error(self.request, _("PayU Transaction Failed. "
        "If you believe that it has been sucessfully transferred. Contact Us"))
        return reverse('basket:summary')


class PlaceOrderResponseView(PaymentDetailsView):
    template_name_preview = 'payuin/preview.html'
    preview = True

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PlaceOrderResponseView, self).dispatch(request, *args, **kwargs)

    @property
    def pre_conditions(self):
        return [] if cartwear.VERSION[:2] >= (0, 8) else ()

    def post(self, request, *args, **kwargs):
        """
        Place an order.

        We fetch the txn details again and then proceed with cartwear's standard
        payment details view for placing the order.
        """
        error_msg = _(
            "A problem occurred communicating with PayU Place Order "
            "- please try again later"
        )
        try:
            self.token = request.POST['txnid']
        except KeyError:
            # Probably suspicious manipulation if we get here
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))

        try:
            self.txn = fetch_transaction_details(self.token)
        except PayUInError:
            # Unable to fetch txn details from PayPal - we have to bail out
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))

        # Reload frozen basket which is specified in the URL
        self.basket = self.load_frozen_basket(kwargs['basket_id'])
        if not self.basket:
            messages.error(self.request, error_msg)
            return HttpResponseRedirect(reverse('basket:summary'))


        shipping_address = self.get_shipping_address(self.basket)
        shipping_method = self.get_shipping_method(
                                                self.basket,
                                                shipping_address
                                            )
        submission = self.build_submission(basket=self.basket,
                                           shipping_address=shipping_address,
                                           shipping_method= shipping_method
                                           )
        return self.submit(**submission)


    def get_context_data(self, **kwargs):
        return super(PlaceOrderResponseView, self).get_context_data(**kwargs)

    def build_submission(self, **kwargs):
        # Pass the user email so it can be stored with the order
        submission = super(
            PlaceOrderResponseView, self).build_submission(**kwargs)
        submission['payment_kwargs']['source'] = "PayU"
        submission['payment_kwargs']['txn'] = self.txn
        return submission


    # Warning: This method can be removed when we drop support for Cartwear 0.6
    def get_error_response(self):
        # We bypass the normal session checks for shipping address and shipping
        # method as they don't apply here.
        pass


    def load_frozen_basket(self, basket_id):
        # Lookup the frozen basket that this txn corresponds to
        try:
            basket = Basket.objects.get(id=basket_id, status=Basket.FROZEN)
        except Basket.DoesNotExist:
            return None

        # Assign strategy to basket instance
        if Selector:
            basket.strategy = Selector().strategy(self.request)

        return basket

    def handle_payment(self, order_number, total, **kwargs):
        """
        Complete payment with PayPal - this calls the 'DoExpressCheckout'
        method to capture the money from the initial transaction.
        """
        try:
            confirm_txn = confirm_transaction(self.token,self.txn.amt)
        except PayUInError:
            raise UnableToTakePayment()
        if not confirm_txn.is_successful:
            raise UnableToTakePayment()

        # Record payment source and event
        source_type, is_created = SourceType.objects.get_or_create(
            name='PayU')
        source = Source(source_type=source_type,
                        currency='INR',
                        reference=self.txn,
                        amount_allocated=self.txn.amt,
                        amount_debited=self.txn.amt)
        self.add_payment_source(source)
        self.add_payment_event('Settled', self.txn.amt,
                               reference = self.txn)

    def get_shipping_method(self, basket, shipping_address=None, **kwargs):
        """
        Return the shipping method used
        """
        if not basket.is_shipping_required():
            return NoShippingRequired()

        # Instantiate a new FixedPrice shipping method instance
        charge_incl_tax = D('0.00')

        # Assume no tax for now
        charge_excl_tax = charge_incl_tax
        method = FixedPrice(charge_excl_tax, charge_incl_tax)
        name = "Standard Shipping"

        if not name:
            session_method = super(SuccessResponseView, self).get_shipping_method(
                basket, shipping_address, **kwargs)
            if session_method:
                method.name = session_method.name
        else:
            method.name = name
        return method

# Upgrading notes: when we drop support for Cartwear 0.6, this class can be
# refactored to pass variables around more explicitly (instead of assigning
# things to self so they are accessible in a later method).
class SuccessResponseView(PaymentDetailsView):
    template_name_preview = 'payuin/preview.html'
    preview = True

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SuccessResponseView, self).dispatch(request, *args, **kwargs)

    @property
    def pre_conditions(self):
        return [] if cartwear.VERSION[:2] >= (0, 8) else ()

    def post(self, request, *args, **kwargs):
        """
        Fetch details about the successful transaction from PayU In.  We use
        these details to show a preview of the order with a 'submit' button to
        place it.
        """
        try:
            self.txnid = request.POST['txnid']
            transaction = PayUInTransaction.objects.get(txnid=self.txnid)
        except KeyError:
            # Manipulation - redirect to basket page with warning message
            logger.warning("Missing Transaction ID params on success response page")
            messages.error(
                self.request,
                _("Unable to determine PayUIn transaction details"))
            return HttpResponseRedirect(reverse('basket:summary'))
        except ObjectDoesNotExist:
            logger.warning("Missing Transaction ID on success response page")
            messages.error(self.request, _("Unable to determine  Transaction ID"))
            return HttpResponseRedirect(reverse('basket:summary'))
        transaction.raw_response = json.dumps(request.POST)
        transaction.save()

        self.txn = fetch_transaction_details(self.txnid)

        # Reload frozen basket which is specified in the URL
        kwargs['basket'] = self.load_frozen_basket(int(transaction.basket))
        if not kwargs['basket']:
            logger.warning(
                "Unable to load frozen basket with ID %s", kwargs['basket_id'])
            messages.error(
                self.request,
                _("No basket was found that corresponds to your "
                  "PayUIn transaction"))
            return HttpResponseRedirect(reverse('basket:summary'))

        logger.info(
            "Basket #%s - showing preview with and transaction ID %s",
            kwargs['basket'].id, transaction.txnid)

        return self.get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):

        return super(SuccessResponseView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(SuccessResponseView, self).get_context_data(**kwargs)

        if not self.txnid:
            return ctx

        # This context generation only runs when in preview mode
        ctx.update({
            'txnid': self.txnid,
            'payuin_basket': self.txn.basket,
            'payuin_amount': D(self.txn.amt),
        })

        return ctx

    def load_frozen_basket(self, basket_id):
        # Lookup the frozen basket that this txn corresponds to
        try:
            basket = Basket.objects.get(id=basket_id, status=Basket.FROZEN)
        except Basket.DoesNotExist:
            return None

        # Assign strategy to basket instance
        if Selector:
            basket.strategy = Selector().strategy(self.request)

        return basket
