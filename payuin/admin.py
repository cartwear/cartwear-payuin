from django.contrib import admin
from payuin import models


class PayUInTransactionAdmin(admin.ModelAdmin):
    list_display = [
        'txnid',
        'mihpayid',
        'mode',
        'amt',
        'additional_charges',
        'basket',
        'status',
        'date_created']
    readonly_fields = [
        'txnid',
        'mihpayid',
        'mode',
        'amt',
        'additional_charges',
        'basket',
        'status',
        'error_code',
        'error_message',
        'raw_request',
        'raw_response',
        'date_created',
        'request',
        'response']


admin.site.register(models.PayUInTransaction, PayUInTransactionAdmin)
