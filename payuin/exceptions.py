try:
    from cartwear.apps.payment.exceptions import PaymentError
except ImportError:
    class PaymentError(Exception):
        pass


class PayUInError(PaymentError):
    pass


class PayUInError(PaymentError):
    pass


class EmptyBasketException(Exception):
    pass


class MissingShippingAddressException(Exception):
    pass


class MissingShippingMethodException(Exception):
    pass


class InvalidBasket(Exception):
    """
    For when the user's basket can't be submitted (eg it has zero cost)

    The message of this exception is shown to the customer.
    """
