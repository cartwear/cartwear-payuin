from __future__ import unicode_literals
import requests
import time

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.exceptions import ObjectDoesNotExist
from django.utils.http import urlencode
from django.core.urlresolvers import reverse
from django.utils import six
from django.utils.six.moves.urllib.parse import parse_qs
from django.utils.translation import ugettext as _
from django.template.defaultfilters import truncatewords, striptags
from localflavor.us import us_states
from hashlib import sha512

import logging
from decimal import Decimal as D

from payuin import models
from payuin import exceptions
import json

class fakefloat(float):
    def __init__(self, value):
        self._value = value
    def __repr__(self):
        return str(self._value)

def defaultencode(o):
    if isinstance(o, D):
        # Subclass float with custom repr?
        return fakefloat(o)
    raise TypeError(repr(o) + " is not JSON serializable")

# PayUIn methods

logger = logging.getLogger('payuin')

VERIFY_PAYUIN_METHOD = "verify_payment"

def get_post_url():
    if getattr(settings, 'PAYUIN_SANDBOX_MODE', True):
        url = 'https://test.payu.in/merchant/postservice.php?form=2'
    else:
        url = 'https://info.payu.in/merchant/postservice.php?form=2'
    return url


def _format_currency(amt):
    return amt.quantize(D('0.01'))


def set_txn(basket, shipping_methods, currency, return_url, failure_url, cancel_url,
            update_url=None, user=None, user_address=None, shipping_method=None,
            shipping_address=None, no_shipping=False, payuin_params=None):
    """
    Register the transaction with PayUIn to get a token which we use in the
    redirect URL.  This is the 'SetExpressCheckout' from their documentation.

    There are quite a few options that can be passed to PayUIn to configure
    this request - most are controlled by PAYUIN_* settings.
    """

    _params = {
        'key':getattr(settings, 'PAYUIN_API_MERCHANT_KEY', 'C0Ds8q')
    }

    # Boolean values become integers
    _params.update((k, int(v)) for k, v in _params.items() if isinstance(v, bool))

    # Remove None values
    params = dict((k, v) for k, v in _params.items() if v is not None)

    # PayUIn have an upper limit on transactions.  It's in dollars which is a
    # fiddly to work with.  Lazy solution - only check when dollars are used as
    # the PayUIn currency.
    amount = basket.total_incl_tax

    if amount <= 0:
        msg = 'The basket total is zero so no payment is required'
        logger.error(msg)
        raise exceptions.InvalidBasket(_(msg))

    # PAYMENTREQUEST_0_AMT should include tax, shipping and handling
    params.update({
        'amount': _format_currency(amount),
        'surl': return_url,
        'furl': failure_url,
        'curl': cancel_url,
    })

    productinfo = ""
    for index,line in enumerate(basket.all_lines()):
        product = line.product
        productinfo = str(product.id) + ":" + str(line.quantity) + "|"
    params['productinfo'] = productinfo

    # Contact details and address details - we provide these as it would make
    # the PayUIn registration process smoother is the user doesn't already have
    # an account.
    if user:
        params['email'] = user.email
        params['firstname'] = user.first_name
        params['lastname'] = user.last_name
    if user_address:
        params['phone'] = user_address.phone_number.national_number
        params['firstname'] = user_address.first_name
        params['lastname'] = user_address.last_name

    # Shipping details (if already set) - we override the SHIPTO* fields and
    # set a flag to indicate that these can't be altered on the PayUIn side.
    if shipping_address:
        params['phone'] = shipping_address.phone_number.national_number
        params['firstname'] = shipping_address.first_name
        params['lastname'] = shipping_address.last_name

    if params['phone'] is None:
        raise exceptions.PayUInError("PayU needs phone number for payment.")

    # Todo: Shipping charges
    txn = models.PayUInTransaction(
        basket = str(basket.id),
        raw_request = json.dumps(params, default=defaultencode)
    )
    txn.save()

    params = {'txnid':txn.txnid}

    url = reverse('payuin-redirect-payment')
    return '%s?%s' % (url, urlencode(params))


def get_txn(txnid):
    """
    Fetch details of a transaction from PayUIn using the txnid as
    an identifier.
    """
    command = VERIFY_PAYUIN_METHOD
    hashString = sha512(settings.PAYUIN_API_MERCHANT_KEY+"|"+command+"|"+txnid+"|"
                 +settings.PAYUIN_API_SALT).hexdigest().lower()
    params = {
        'key': settings.PAYUIN_API_MERCHANT_KEY,
        'command': command,
        'var1': txnid,
        'hash':hashString
    }

    url = get_post_url()

    # Print easy-to-read version of params for debugging
    param_str = "\n".join(["%s: %s" % x for x in sorted(params.items())])
    logger.debug("Making %s request to %s with params:\n%s", command, url,
                 param_str)

    # Make HTTP request
    start_time = time.time()
    response = requests.post(url, params)
    if response.status_code != requests.codes.ok:
        raise exceptions.PayUInError("Unable to communicate with PayU")
    logger.debug("Response is:\n%s", str(response.text))

    # Get Transaction
    try:
        txn = models.PayUInTransaction.objects.get(txnid=txnid)
    except ObjectDoesNotExist:
        raise exceptions.PayUInError("Transaction ID Doesn't Exist")

    trans_response = json.loads(response.text)
    transaction_details = trans_response['transaction_details']
    pairs = transaction_details[txnid]
    txn.raw_response = response.text
    txn.status = pairs['status']

    # Record transaction data - we save this model whether the txn
    # was successful or not
    if txn.status:
        if command == VERIFY_PAYUIN_METHOD:
            txn.mihpayid = pairs['mihpayid']
            txn.amt = D(pairs['amt'])
            txn.mode = pairs['mode']
            txn.additional_charges = D(pairs['additional_charges'])
    else:
        # There can be more than one error, each with its own number.
        if 'error_code' in pairs:
            txn.error_code = pairs['error_code']
        if 'error_Message' in pairs:
            txn.error_message = pairs['error_Message']

    txn.save()
    return txn


def do_txn(token, amount):
    """
    DoExpressCheckoutPayment
    """
    return get_txn(token)
