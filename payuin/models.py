from __future__ import unicode_literals
import re

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, pgettext_lazy

from payuin import abstract_models

import uuid

class UUIDField(models.CharField) :

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = kwargs.get('max_length', 64 )
        kwargs['blank'] = True
        models.CharField.__init__(self, *args, **kwargs)

    def pre_save(self, model_instance, add):
        if add :
            value = str(uuid.uuid4())
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(models.CharField, self).pre_save(model_instance, add)


@python_2_unicode_compatible
class PayUInTransaction(abstract_models.ResponseModel):

    # The PayUIn method and version used
    txnid = UUIDField(editable=False, db_index=True)
    mihpayid = models.CharField(max_length=20, db_index=True, blank = True)

    CC, DC, NB, CASH, EMI, IVR, COD = ('CC', 'DC', 'NB', 'CASH', 'EMI', 'IVR', 'COD')

    MODE_CHOICES = (
        (CC, _("Credit Card")),
        (DC, _("Debit Card")),
        (NB, _("Net Banking")),
        (CASH, _("Cash Card")),
        (EMI, _("Every Month Installments")),
        (IVR, _("Interactive voice response")),
        (COD, _("Cash on Delivery")),
    )

    mode = models.CharField(pgettext_lazy(u"Mode of payment", u"Payment Method"),
        max_length=64, choices=MODE_CHOICES, blank=True)
    amt = models.DecimalField(max_digits=12, decimal_places=2, null=True,
                                 blank=True)
    additional_charges = models.DecimalField(max_digits=12, decimal_places=2, null=True,
                                 blank=True)
    basket = models.CharField(max_length=20, db_index=True, blank = True)
    SUCCESS, FAILURE, PENDING = ("success","failure","pending")
    STATUS_CHOICES = (
        (SUCCESS, _("Success")),
        (FAILURE, _("Failure")),
        (PENDING, _("Pending")),
    )

    status = models.CharField(
        pgettext_lazy(u"Treatment Status with PayU", u"Transaction Status"),
        max_length=20, choices=STATUS_CHOICES, blank=True)


    error_code = models.CharField(max_length=32, null=True, blank=True)
    error_message = models.CharField(max_length=256, null=True, blank=True)

    class Meta:
        ordering = ('-date_created',)
        app_label = 'payuin'

    def save(self, *args, **kwargs):
        self.raw_request = re.sub(r'PWD=\d+&', 'PWD=XXXXXX&', self.raw_request)
        return super(PayUInTransaction, self).save(*args, **kwargs)

    @property
    def is_successful(self):
        return self.status == self.SUCCESS

    def __str__(self):
        return 'Transaction ID: %s' % (self.txnid)
